/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.Contract;
import com.selfxdsd.api.Contributor;
import com.selfxdsd.api.User;
import com.selfxdsd.rest.output.JsonContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Contracts of the authenticated User.
 * @author Mihai Andronache (amihaiemil@gmail.com)
 * @version $Id$
 * @since 0.0.1
 */
@RestController
@RequestMapping("/contracts")
public class ContractsApi extends BaseRestController {

    /**
     * Authenticated user.
     */
    private User user;

    /**
     * Constructor.
     * @param user Authenticated User.
     */
    @Autowired
    public ContractsApi(final User user) {
        this.user = user;
    }

    /**
     * Get a Contract of the authenticated User.<br><br>
     *
     * We expect as input parameters the owner and repo name (repoFullName)
     * and the role and use the username and provider of the authenticated User.
     *
     * @return Contract or 204 NO CONTENT if not found.
     * @param owner Login of the user or organization who owns the repo.
     * @param name Repo name.
     * @param role Role of the contributor in the project.
     */
    @GetMapping(
        value = "/{owner}/{name}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> contract(
        @PathVariable final String owner,
        @PathVariable final String name,
        @RequestParam("role") final String role
    ){
        final ResponseEntity<String> resp;
        final Contributor contributor = this.user.asContributor();
        if(contributor == null) {
            resp = ResponseEntity.noContent().build();
        } else {
            final Contract found = contributor.contract(
                owner + "/" + name,
                contributor.provider(),
                role
            );
            if(found == null) {
                resp = ResponseEntity.noContent().build();
            } else {
                resp = ResponseEntity.ok(
                    new JsonContract(found).toString()
                );
            }
        }
        return resp;
    }
}
