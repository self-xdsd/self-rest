/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.Contract;
import com.selfxdsd.api.Invoice;
import com.selfxdsd.api.Invoices;
import com.selfxdsd.api.User;
import com.selfxdsd.rest.output.JsonInvoice;
import com.selfxdsd.rest.output.JsonInvoices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Contributor Invoices.
 * @author criske
 * @version $Id$
 * @since 0.0.1
 */
@RestController
@RequestMapping("/contracts/{owner}/{name}/invoices/{role}")
public class ContributorInvoicesApi extends BaseRestController {

    /**
     * Authenticated user.
     */
    private final User user;

    /**
     * Constructor.
     * @param user Authenticated User.
     */
    @Autowired
    public ContributorInvoicesApi(final User user) {
        this.user = user;
    }

    /**
     * Get one Invoice by its ID.
     * @param owner Login of the user or organization who owns the repo.
     * @param name Repo name.
     * @param role Role of the contributor in contract.
     * @param invoiceId The ID.
     * @return The found Invoice in JSON or 204 NO CONTENT.
     * @checkstyle ParameterNumber (30 lines).
     */
    @GetMapping(
        value = "/{invoiceId}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> invoice(
        @PathVariable final String owner,
        @PathVariable final String name,
        @PathVariable final String role,
        @PathVariable final int invoiceId
    ) {
        final ResponseEntity<String> resp;
        final Contract contract = this.user.asContributor()
            .contract(owner + "/" + name, this.user.provider().name(), role);
        if (contract != null) {
            final Invoice invoice = contract
                .invoices().getById(invoiceId);
            if (invoice != null) {
                resp = ResponseEntity.ok(new JsonInvoice(invoice).toString());
            } else {
                resp =  ResponseEntity.noContent().build();
            }
        } else {
            resp = ResponseEntity.noContent().build();
        }
        return resp;
    }

    /**
     * Get all Contributor Invoices of a Contract as JsonArray.
     * @param owner Login of the user or organization who owns the repo.
     * @param name Repo name.
     * @param role Role of the contributor in contract.
     * @return JsonArray containing Invoices.
     */
    @GetMapping(
        value = "/all",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> all(
        @PathVariable final String owner,
        @PathVariable final String name,
        @PathVariable final String role
    ) {
        final ResponseEntity<String> resp;
        final Contract contract = this.user.asContributor()
            .contract(owner + "/" + name, this.user.provider().name(), role);
        if (contract != null) {
            final Invoices invoices = contract.invoices();
            resp = ResponseEntity.ok(new JsonInvoices(invoices).toString());
        } else {
            resp = ResponseEntity.noContent().build();
        }
        return resp;
    }
}
