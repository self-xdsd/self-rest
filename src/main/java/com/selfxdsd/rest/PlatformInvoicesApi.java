/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.*;
import com.selfxdsd.rest.output.JsonPlatformInvoice;
import com.selfxdsd.rest.output.JsonPlatformInvoices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Platform Invoices.
 * @author Mihai Andronache (amihaiemil@gmail.com)
 * @version $Id$
 * @since 0.0.1
 */
@RestController
@RequestMapping("/admin/platformInvoices")
public class PlatformInvoicesApi extends BaseRestController {

    /**
     * Authenticated user.
     */
    private User user;

    /**
     * Constructor.
     * @param user Authenticated User.
     */
    @Autowired
    public PlatformInvoicesApi(final User user) {
        this.user = user;
    }

    /**
     * Get all PlatformInvoices in Self as JsonArray.
     * @return JsonArray containing PlatformInvoices.
     */
    @GetMapping(
        value = "/all",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> all() {
        final ResponseEntity<String> resp;
        final Admin admin = this.user.asAdmin();
        if(admin == null) {
            resp = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } else {
            resp = ResponseEntity.ok(
                new JsonPlatformInvoices(
                    admin.platformInvoices()
                ).toString()
            );
        }
        return resp;
    }

    /**
     * Get one PlatformInvoice by its ID.
     * @param platformInvoiceId The ID.
     * @return The found PlatformInvoice in JSON or 204 NO CONTENT. If the
     *  authenticated User is not an Admin, the response is FORBIDDEN.
     */
    @GetMapping(
        value = "/{platformInvoiceId}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> platformInvoice(
        @PathVariable final int platformInvoiceId
    ){
        final ResponseEntity<String> resp;
        final Admin admin = this.user.asAdmin();
        if(admin == null) {
            resp = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        } else {
            final PlatformInvoice invoice = admin.platformInvoices().getById(
                platformInvoiceId
            );
            if(invoice == null) {
                resp = ResponseEntity.noContent().build();
            } else {
                resp = ResponseEntity.ok(
                    new JsonPlatformInvoice(invoice).toString()
                );
            }
        }
        return resp;
    }
}
