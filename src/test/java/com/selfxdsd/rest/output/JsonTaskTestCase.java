/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest.output;

import com.selfxdsd.api.Contract;
import com.selfxdsd.api.Project;
import com.selfxdsd.api.Provider;
import com.selfxdsd.api.Task;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.json.Json;

/**
 * Unit tests for {@link JsonTask}.
 * @author criske
 * @version $Id$
 * @since 0.0.1
 */
public final class JsonTaskTestCase {

    /**
     * Represents a Task as Json for the authenticated User..
     */
    @Test
    public void representsTaskAsJson() {
        final Task task = Mockito.mock(Task.class);
        final Project project = Mockito.mock(Project.class);

        Mockito.when(task.issueId()).thenReturn("#1");
        Mockito.when(task.role()).thenReturn(Contract.Roles.DEV);
        Mockito.when(task.isPullRequest()).thenReturn(false);
        Mockito.when(task.estimation()).thenReturn(60);
        Mockito.when(task.project()).thenReturn(project);
        Mockito.when(project.repoFullName()).thenReturn("mihai/test");
        Mockito.when(project.provider()).thenReturn(Provider.Names.GITHUB);

        MatcherAssert.assertThat(
            new JsonTask(task),
            Matchers.equalTo(
                Json.createObjectBuilder()
                    .add("issueId", "#1")
                    .add("isPullRequest", task.isPullRequest())
                    .add("role", Contract.Roles.DEV)
                    .add("repoFullName", "mihai/test")
                    .add("provider", Provider.Names.GITHUB)
                    .add("estimation", "60 min.")
                    .build()
            )
        );
    }

}