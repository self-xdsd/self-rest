/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest.output;

import com.selfxdsd.api.Contract;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.json.Json;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Unit tests for {@link JsonContract}.
 * @author Mihai Andronache (amihaiemil@gmail.com)
 * @version $Id$
 * @since 0.0.1
 */
public final class JsonContractTestCase {

    /**
     * JsonContract is the Contract in JSON format.
     */
    @Test
    public void representsContractAsJsonWithoutRemovalDate() {
        final Contract contract = Mockito.mock(Contract.class);
        final Contract.Id contractId = new Contract.Id(
            "mihai/test",
            "amihaiemil",
            "github",
            "DEV"
        );
        Mockito.when(contract.contractId()).thenReturn(contractId);
        Mockito.when(contract.hourlyRate())
            .thenReturn(BigDecimal.valueOf(1500));

        MatcherAssert.assertThat(
            new JsonContract(contract),
            Matchers.equalTo(
                Json.createObjectBuilder()
                    .add("repoFullName", "mihai/test")
                    .add("provider", "github")
                    .add("username", "amihaiemil")
                    .add("role", "DEV")
                    .add("hourlyRate", 1500)
                    .add("currency", "EUR")
                    .add("unit", "cents")
                    .add("markedForRemoval", "null")
                    .build()
            )
        );
    }

    /**
     * JsonContract is the Contract in JSON format.
     */
    @Test
    public void representsContractAsJsonWithRemovalDate() {
        final Contract contract = Mockito.mock(Contract.class);
        final Contract.Id contractId = new Contract.Id(
            "mihai/test",
            "amihaiemil",
            "github",
            "DEV"
        );
        Mockito.when(contract.contractId()).thenReturn(contractId);
        Mockito.when(contract.hourlyRate())
            .thenReturn(BigDecimal.valueOf(1500));
        Mockito.when(contract.markedForRemoval()).thenReturn(
            LocalDateTime.of(2021, 2, 6, 0, 0, 0)
        );

        MatcherAssert.assertThat(
            new JsonContract(contract),
            Matchers.equalTo(
                Json.createObjectBuilder()
                    .add("repoFullName", "mihai/test")
                    .add("provider", "github")
                    .add("username", "amihaiemil")
                    .add("role", "DEV")
                    .add("hourlyRate", 1500)
                    .add("currency", "EUR")
                    .add("unit", "cents")
                    .add("markedForRemoval", "2021-02-06T00:00")
                    .build()
            )
        );
    }
}
