/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest.output;

import com.selfxdsd.api.Contract;
import com.selfxdsd.api.Invoice;
import com.selfxdsd.api.Invoices;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.json.Json;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Unit tests for {@link JsonInvoices}.
 * @author criske
 * @version $Id$
 * @since 0.0.1
 * @checkstyle LineLength (1000 lines)
 * @checkstyle ExecutableStatementCount (1000 lines)
 */
public final class JsonInvoicesTestCase {

    /**
     * It can represent Invoices as JsonArray.
     */
    @Test
    public void representsInvoicesAsJson() {
        final Invoice invoice = Mockito.mock(Invoice.class);
        final Invoices invoices = Mockito.mock(Invoices.class);
        final Contract contract = Mockito.mock(Contract.class);

        Mockito.when(invoice.invoiceId()).thenReturn(1);
        Mockito.when(invoice.createdAt()).thenReturn(LocalDateTime
            .of(2021, 2, 8, 0, 0, 0));
        Mockito.when(invoice.billedBy()).thenReturn("XD Tech S.R.L.");
        Mockito.when(invoice.billedTo()).thenReturn("Mihai Andronache");
        Mockito.when(invoice.commission()).thenReturn(BigDecimal.valueOf(100));
        Mockito.when(invoice.totalAmount()).thenReturn(BigDecimal.valueOf(119));
        Mockito.when(invoice.isPaid()).thenReturn(true);
        Mockito.when(invoice.transactionId()).thenReturn("transaction123");
        Mockito.when(invoice.paymentTime()).thenReturn(LocalDateTime
            .of(2021, 3, 8, 0, 0, 0));
        Mockito.when(invoice.contract()).thenReturn(contract);
        Mockito.when(contract.contractId()).thenReturn(new Contract.Id(
            "mihai/test",
            "mihai",
            "github",
            "dev")
        );
        Mockito.when(invoices.spliterator()).thenReturn(List.of(invoice)
            .spliterator());

        final JsonInvoices json = new JsonInvoices(invoices);
        MatcherAssert.assertThat(
            json,
            Matchers.equalTo(
                Json.createArrayBuilder().add(
                    Json.createObjectBuilder()
                        .add("id", 1)
                        .add("createdAt", "2021-02-08T00:00")
                        .add("billedBy", "XD Tech S.R.L.")
                        .add("billedTo", "Mihai Andronache")
                        .add("commission", 100)
                        .add("totalAmount", 119)
                        .add("currency", "EUR")
                        .add("unit", "cents")
                        .add("payment", Json.createObjectBuilder()
                            .add("transactionId", "transaction123")
                            .add("paidAt", "2021-03-08T00:00")
                            .build())
                        .add("contract", "/contracts/mihai/test?role=dev")
                        .build()
                ).build())
        );
    }
}